import React from 'react';

import './glassmorphismLogin.css';

interface glassmorphismLoginProps {
}

export const GlassmorphismLogin = () => (
  <article>
    <section className="gmlogin-section">
      <div className="gmlogin-color"></div>
      <div className="gmlogin-color"></div>
      <div className="gmlogin-color"></div>
      <div className="gmlogin-box">
        <div className="gmlogin-square"></div>
        <div className="gmlogin-square"></div>
        <div className="gmlogin-square"></div>
        <div className="gmlogin-square"></div>
        <div className="gmlogin-square"></div>
        <div className="gmlogin-container">
          <div className="gmlogin-form">
            <h2>Login Form</h2>
            <form>
              <div className="gmlogin-inputBox">
                <input type="text" placeholder="Username"></input>
              </div>
              <div className="gmlogin-inputBox">
                <input type="password" placeholder="Password"></input>
              </div>
              <div className="gmlogin-inputBox">
                <input type="submit" value="Login"></input>
              </div>
              <p className="gmlogin-forget">Forget Password ? <a href="#">Click Here</a></p>
              <p className="gmlogin-signup">Don't have an account ? <a href="#">Sign up</a></p>
            </form>
          </div>
        </div>
      </div>
    </section>
  </article>
);
