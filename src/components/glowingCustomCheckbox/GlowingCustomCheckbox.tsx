import React from 'react';

import './glowingCustomCheckbox.css';

export const GlowingCustomCheckbox = () => (
  <div className="glowingcustomcheckbox-center">
    <input className="glowingcustomcheckbox-input" type="checkbox" name=""/>
  </div>
);
