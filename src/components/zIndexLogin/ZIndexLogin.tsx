import React from 'react';

// get our fontawesome imports
import { faFacebook, faInstagram, faLinkedin, faTwitter, faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './zIndexLogin.css';

interface ZIndexLoginProps {
}

export const ZIndexLogin = () => (
  <article>
    <section className="zindexlogin-section">
      <div className="color"></div>
      <div className="color"></div>
      <div className="color"></div>
      <ul>
        <li><a href="#">
          <FontAwesomeIcon icon={faFacebook} />
        </a></li>
        <li><a href="#">
          <FontAwesomeIcon icon={faTwitter} />
        </a></li>
        <li><a href="#">
          <FontAwesomeIcon icon={faInstagram} />
        </a></li>
        <li><a href="#">
          <FontAwesomeIcon icon={faLinkedin} />
        </a></li>
        <li><a href="#">
          <FontAwesomeIcon icon={faWhatsapp} />
        </a></li>
      </ul>
    </section>
  </article>
);
