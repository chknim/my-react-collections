import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { GlassmorphismLogin } from '../components/glassmorphismLogin/GlassmorphismLogin';

export default {
  title: 'CSS/GlassmorphismLogin',
  component: GlassmorphismLogin,
} as ComponentMeta<typeof GlassmorphismLogin>;

const Template: ComponentStory<typeof GlassmorphismLogin> = (args) => <GlassmorphismLogin />;

export const Default = Template.bind({});
Default.args = {};
