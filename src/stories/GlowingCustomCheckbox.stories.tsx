import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { GlowingCustomCheckbox } from '../components/glowingCustomCheckbox/GlowingCustomCheckbox';

export default {
  title: 'CSS/GlowingCustomCheckbox',
  component: GlowingCustomCheckbox,
} as ComponentMeta<typeof GlowingCustomCheckbox>;

const Template: ComponentStory<typeof GlowingCustomCheckbox> = (args) => <GlowingCustomCheckbox />;

export const Default = Template.bind({});
Default.args = {};

