import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { ZIndexLogin } from '../components/zIndexLogin/ZIndexLogin';

export default {
  title: 'CSS/ZIndexLogin',
  component: ZIndexLogin,
} as ComponentMeta<typeof ZIndexLogin>;

const Template: ComponentStory<typeof ZIndexLogin> = (args) => <ZIndexLogin />;

export const Default = Template.bind({});
Default.args = {};
